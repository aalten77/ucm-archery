<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/darkly/bootstrap.min.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <title>UC Merced Archery</title>
	
	
	
	
</head>
<style type="text/css">
    body{ background: #DCDCDC;
    color: black;
    }
   
    .navbar-default .navbar-brand:hover,
    .navbar-default .navbar-brand:focus {
    color: #FFFFFF;
    }
    .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
    color: #0099CC;
    }
    .nopadding {
    padding: 0 !important;
    margin: 0 !important;
    }
    .borderless table{
    border-top-style: none;
    border-left-style: none;
    border-right-style: none;
    border-bottom-style: none;
    cellspacing: 10px;
    }
    .navbar-brand{
    padding: 0;
    margin: 0;
    }
    .navbar-brand img
    {
    max-height: 100%;
    }
</style>
<body>
<!-- NavBar -->
<nav class="navbar navbar-default">
    <div class="container-fluid">

        <!--Logo -->
        <div class="navbar-header">
            <a href="#" class="navbar-brand"><img class="img-responsive" src="images\UCMArchery.jpg"></a>
        </div>

		
        <!-- Menu Items -->
        <div>
            <ul class="nav navbar-nav">
                <li><a href="index.html">Home</a></li>
                <li><a href="clubinfo.html">Club Info</a></li>
                <li class="active"><a href="Join.php">Join</a></li>
				<li><a href="login.php">Login</a></li>
                <li><a href="photos.html">Photos</a></li>
                <li><a href="budget.php">Budget</a></li>
                <li><a href="tourneyresults.php">Tournament Results</a></li>
				<li><a href="equipment.php">Equipment</a></li>
                <li><a href="aboutus.html">About Us</a></li>
				<li> 
				<form method="post" id="logout" action='logout.php' >
                    <input id='button' class="btn btn-info btn-sm" type='submit' name='button' value='LOGOUT'/>
				</form>
				</li>
            </ul>
        </div>
		
    </div>
</nav>

<div class="container-fluid">

</div>

<!--<p>Page under construction.</p>-->
<div>
<?php 
$namejoin = $usernamejoin = $passjoin = $emailjoin;
$owns;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
   $namejoin = test_input($_POST["namejoin"]);
   $usernamejoin = test_input($_POST["usernamejoin"]);
   $passjoin = test_input($_POST["passjoin"]);
   $emailjoin=  test_input($_POST["emailjoin"]);
	}

	function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
	
   ?>
<form method="post" id="userjoin" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

Name :   <input type="text" name="namejoin" />
	<P></p>
	User Name :   <input type="text" name="usernamejoin" />

	<P></p>
	Email  :   <input type="text" name="emailjoin" />

	<P></p>
Password : <input type="text" name="passjoin"/>
	
	<!--
	<br>
	 <input type="checkbox" name="owns" value = "1"> I have equipment<br>
	-->
<p></P>
	<!--<input type='submit' name='submit' value='Join' /> -->
	  <button type="submit" class="btn btn-primary">Join</button>
</form>
	<p></p>
	
	<?php
   class MyDB extends SQLite3
   {
      function __construct()
      {
         $this->open('archerydata.db');
      }
   }
   $db = new MyDB();
   if(!$db){
      echo $db->lastErrorMsg();
   } else {
      echo "Opened database successfully\n";
   }
   
   if ($namejoin) {
   echo "<br>";
   echo "Inserting ... \n";
   echo "<br>";
 ///////////////////////////////////////////////////  
    $sql =<<<EOF
      Insert into member (m_memberkey, m_membername, m_officerkey, m_teamname,
	  m_membertype, m_username, m_password, m_email, m_phonenumber)
	  values (NULL, '$namejoin' , NULL,NULL,NULL,
	  '$usernamejoin','$passjoin', '$emailjoin', NULL) ;
EOF;
    $ret = $db->exec($sql);
   if(!$ret){
   echo "<br>";
      echo $db->lastErrorMsg();
	  echo "<br> You didn't join successfully <br>";
   } else {
      echo "Records created successfully\n";
	  echo "<br>";
   }

}
   echo "<br>";
   if($namejoin){
    echo "selecting...";
	echo "<br>";
   $sql =<<<EOF
      select *  from member
	  where  m_membername = '$namejoin' and
	  m_password = '$passjoin' and m_username = '$usernamejoin' ) ;
EOF;
   $ret = $db->query($sql);
   while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
      echo "m_memberkey = ". $row['m_memberkey'] . "\r\n";
      echo "<br>";
	  echo "m_membername = ". $row['m_membername'] ."\t";
	  echo "<br>";
      echo "m_officerkey = ". $row['m_officerkey'] ."\n";
	  echo "<br>";
      echo "m_email =  ".$row['m_email'] ."\n\n";
	  echo "<br>";
	  echo "<br>";
   }
   echo "Select done successfully\n";
   $db->close();
   }
   
   
?>
</div>
</body>
</html>