SELECT tn_name, tn_location, tn_startdate, tn_enddate, m_membername, a_round, a_gendercategory, a_bowclass, a_place
FROM awards, member, tournament
WHERE a_memberkey = m_memberkey AND tn_tournamentkey = a_tournamentkey AND tn_enddate BETWEEN date('2014-12-31') AND date('2015-12-31');