select sum(b_amount) , year, month
from (
select STRFTIME('%Y', b_date) as year, STRFTIME('%m', b_date) as month , b_amount
from budget
group by year, b_amount, month)
group by month
