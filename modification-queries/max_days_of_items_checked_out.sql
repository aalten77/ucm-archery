SELECT A.eq_type, MAX(A.datediff)
FROM (SELECT eq_type, (JulianDay(eq_checkindate) - JulianDay(eq_checkoutdate)) AS datediff
FROM equipment)A;