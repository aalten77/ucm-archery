UPDATE account
SET  ac_actbal = ac_actbal + (SELECT SUM(tr_amount)  
FROM treasurer, coordinator
WHERE c_action = tr_action AND tr_action = 'WITHDRAW' AND c_approve = 'T');