<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/darkly/bootstrap.min.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


    <title>UC Merced Archery</title>
</head>
<style type="text/css">
    body{ background: #DCDCDC;
    color: black;
    }

    .navbar-default .navbar-brand:hover,
    .navbar-default .navbar-brand:focus {
    color: #FFFFFF;
    }
    .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
    color: #0099CC;
    }
    .nopadding {
    padding: 0 !important;
    margin: 0 !important;
    }

    .navbar-brand{
    padding: 0;
    margin: 0;
    }
    .navbar-brand img
    {
    max-height: 100%;
    }
    .table > thead > tr > th {
        vertical-align: bottom;
        border-bottom: 2px solid #286090;
    }
    .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #286090;
    }
</style>
<body>
    <!-- NavBar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">

            <!--Logo -->
            <div class="navbar-header">
                <a href="#" class="navbar-brand"><img class="img-responsive" src="images\UCMArchery.jpg"></a>
            </div>

            <!-- Menu Items -->
            <div>
                <ul class="nav navbar-nav">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="clubinfo.html">Club Info</a></li>
                    <li><a href="Join.php">Join</a></li>
					<li><a href="login.php">Login</a></li>
                    <li><a href="photos.html">Photos</a></li>
                    <li><a href="budget.php">Budget</a></li>
                    <li class="active"><a href="tourneyresults.php">Tournament Results</a></li>
                    <li><a href="equipment.php">Equipment</a></li>
                    <li><a href="aboutus.html">About Us</a></li>
					<li> 
				<form method="post" id="logout" action='logout.php' >
                    <input id='button' class="btn btn-info btn-sm" type='submit' name='button' value='LOGOUT'/>
				</form>
				
				</li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th>Tournament</th>
                <th>Location</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Member</th>
                <th>Round</th>
                <th>Gender</th>
                <th>Bow Class</th>
                <th>Place</th>
            </tr>
        </thead>
        <tbody>
    <?php


    class MyDB extends SQLite3
    {
        function __construct()
        {
            $this->open('archerydata.db');
        }
    }
    $db = new MyDB();
    if(!$db){
        echo $db->lastErrorMsg();
    }

    $sql =<<<EOF
      SELECT tn_name, tn_location, tn_startdate, tn_enddate, m_membername, a_round, a_gendercategory, a_bowclass, a_place
FROM awards, member, tournament
WHERE a_memberkey = m_memberkey AND tn_tournamentkey = a_tournamentkey AND tn_enddate BETWEEN date('2014-12-31') AND date('2015-12-31');
EOF;


    $ret = $db->query($sql);
    while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
    ?>

        <tr>
            <td><?php echo $row['tn_name']; ?></td>
            <td><?php echo $row['tn_location']; ?></td>
            <td><?php echo $row['tn_startdate']; ?></td>
            <td><?php echo $row['tn_enddate']; ?></td>
            <td><?php echo $row['m_membername']; ?></td>
            <td><?php echo $row['a_round']; ?></td>
            <td><?php echo $row['a_gendercategory']; ?></td>
            <td><?php echo $row['a_bowclass']; ?></td>
            <td><?php echo $row['a_place']; ?></td>
        </tr>

    <?php
    }
    $db->close();

    ?>
        </tbody>
    </table>
    </div>

</body>
</html>