<?php  session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/darkly/bootstrap.min.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <title>UC Merced Archery</title>
</head>
<style type="text/css">
    body{ background: #DCDCDC;
    color: black;
    }
    #map-canvas {
    display:inline-block;
    width: 600px;
    height: 400px;
    margin-left: 1%;
    padding:5px;
    }
    #google-calendar{
    display: inline-block;
    width:200px;
    height:100px;
    padding-top: 0px;
    padding-left: 10px;
    }

    .navbar-default .navbar-brand:hover,
    .navbar-default .navbar-brand:focus {
    color: #FFFFFF;
    }
    .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
    color: #0099CC;
    }
    .nopadding {
    padding: 0 !important;
    margin: 0 !important;
    }
    .borderless table{
    border-top-style: none;
    border-left-style: none;
    border-right-style: none;
    border-bottom-style: none;
    cellspacing: 10px;
    }
    .navbar-brand{
    padding: 0;
    margin: 0;
    }
    .navbar-brand img
    {
    max-height: 100%;
    }
</style>
<body>
<!-- NavBar -->
<nav class="navbar navbar-default">
    <div class="container-fluid">

        <!--Logo -->
        <div class="navbar-header">
            <a href="#" class="navbar-brand"><img class="img-responsive" src="images\UCMArchery.jpg"></a>
        </div>

        <!-- Menu Items -->
        <div>
            <ul class="nav navbar-nav">
                <li><a href="index.html">Home</a></li>
                <li><a href="clubinfo.html">Club Info</a></li>
                <li><a href="Join.php">Join</a></li>
				<li><a href="login.php">Login</a></li>
                <li><a href="photos.html">Photos</a></li>
                <li class="active"><a href="budget.php">Budget</a></li>
                <li><a href="tourneyresults.php">Tournament Results</a></li>
				<li><a href="equipment.php">Equipment</a></li>
                <li><a href="aboutus.html">About Us</a></li>
				<li> 
				<form method="post" id="logout" action='logout.php' >
                    <input id='button' class="btn btn-info btn-sm" type='submit' name='button' value='LOGOUT'/>
				</form>
				</li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">

</div>

<!--<p>Page under construction.</p>-->

<p></p>
<div>
<?php 

$amount=$comment=$type="";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
   $amount = test_input($_POST["amount"]);
   $comment = test_input($_POST["comment"]);
   $type = test_input($_POST["type"]);
	}
	function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
   ?>

<form method="post" id="nameandpass" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">


<p><b>Budget request</b> </p>

<?php
	if(!$_SESSION["m_memberkey"]){
		echo "Please log in first!!!!";
		echo "<br>";
		echo "<br>";
		echo "<br>";
		echo "<br>";
		}
		?>
<p> Please enter a positive amount</p>
Amount :   <input type="text" name="amount" />
	<P></p>
	
<div class="form-group">
    <label for="sel1">Budget Action:</label>
    <select class="form-control" id="sel1" name="type">
        <option value="withdrawal">Request withdrawal</option>
        <option value="deposit">Pay Dues/Donate</option>
    </select>
</div>

<input type="text" name="comment" value="comment"/> 
<p></P>
	<input type='submit' class="btn btn-primary" name='submit' value='submit'  />
</form>
	<?php
	echo $amount . "&" . $comment . "&" . $type;
	?>
	<p></p>
	
	<?php
   class MyDB extends SQLite3
   {
      function __construct()
      {
         $this->open('archerydata.db');
      }
   }
   $db = new MyDB();
   if(!$db){
      echo $db->lastErrorMsg();
   } else {
      echo "Opened database successfully\n";
	  echo "<br>";
   }
   
 ///////////////////////////////////////////////////  
   
   //generate date now
   /*
   $date = getdate(year);
   $date = $date ."-".getdate("mon");
   $date = $date ."-".getdate("mday");
   */
   $date = date("Y-m-d");
   //echo $date; 
   echo "<br>";
   
   
	if($amount && $_SESSION["m_memberkey"]) {
		if($type== "withdrawal"){
		$sql =<<<EOF
      insert into budget(b_amount, b_actiontype, b_comment, b_date)
	  values(-$amount, '$type', '$comment', '$date');
EOF;
}
		else{
		$sql =<<<EOF
      insert into budget(b_amount, b_actiontype, b_comment, b_date)
	  values($amount, '$type', '$comment','$date');
EOF;
}
   $ret = $db->exec($sql);
  if(!$ret){
   echo "<br>";
      echo $db->lastErrorMsg();
	  echo "<br> You didn't add to the budget, sorry <br>";
   } else {
      echo "Record created successfully";
	  echo "<br>";
   }
   $db->close();   
   
}
?>
</div>

</body>
</html>