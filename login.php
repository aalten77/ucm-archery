<?php  session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/darkly/bootstrap.min.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <title>UC Merced Archery</title>
	
	
	

</head>
<style type="text/css">
    body{ background: #DCDCDC;
    color: black;
    }
    #map-canvas {
    display:inline-block;
    width: 600px;
    height: 400px;
    margin-left: 1%;
    padding:5px;
    }
    #google-calendar{
    display: inline-block;
    width:200px;
    height:100px;
    padding-top: 0px;
    padding-left: 10px;
    }

    .navbar-default .navbar-brand:hover,
    .navbar-default .navbar-brand:focus {
    color: #FFFFFF;
    }
    .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
    color: #0099CC;
    }
    .nopadding {
    padding: 0 !important;
    margin: 0 !important;
    }
    .borderless table{
    border-top-style: none;
    border-left-style: none;
    border-right-style: none;
    border-bottom-style: none;
    cellspacing: 10px;
    }
    .navbar-brand{
    padding: 0;
    margin: 0;
    }
    .navbar-brand img
    {
    max-height: 100%;
    }
</style>
<body>
<!-- NavBar -->
<nav class="navbar navbar-default">
    <div class="container-fluid">

        <!--Logo -->
        <div class="navbar-header">
            <a href="#" class="navbar-brand"><img class="img-responsive" src="images\UCMArchery.jpg"></a>
        </div>

       <!-- Menu Items -->
        <div>
            <ul class="nav navbar-nav">
                <li><a href="index.html">Home</a></li>
                <li><a href="clubinfo.html">Club Info</a></li>
                <li><a href="Join.php">Join</a></li>
				<li class="active"><a href="login.php">Login</a></li>
                <li><a href="photos.html">Photos</a></li>
                <li><a href="budget.php">Budget</a></li>
                <li><a href="tourneyresults.php">Tournament Results</a></li>
				<li><a href="equipment.php">Equipment</a></li>
                <li><a href="aboutus.html">About Us</a></li>
				<li> 
				<form method="post" id="logout" action='logout.php' > 
				<?php  echo "hello #" . $_SESSION["m_memberkey"] . ", " . $_SESSION["m_membername"] ;?>
				<input id='button' class="btn btn-info btn-sm" type='submit' name='button' value='LOGOUT'/>
				</form>
				</li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">

</div>

<!--<p>Page under construction.</p>-->

<p></p>
<div>
<?php 

$name = $email = $gender = $comment = $website = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
   $name = test_input($_POST["name"]);
   $pass = test_input($_POST["pass"]);
	}
	function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
   ?>

<form method="post" id="nameandpass" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
Name :   <input type="text" name="name" />

	<P></p>
Password : <input type="text" name="pass"/>
	
<p></P>
	<input type="submit" class="btn btn-primary" name="submit" value="LOGIN"  />
	
</form>


	<?php
	/*
	if( $name) {
		echo "name: ";
		echo $name;
		echo '<br>';
		echo "password: ";
		echo $pass;
		}*/
	?>
	<p></p>
	
	<?php
   class MyDB extends SQLite3
   {
      function __construct()
      {
         $this->open('archerydata.db');
      }
   }
   $db = new MyDB();
   if(!$db){
      echo $db->lastErrorMsg();
   } else {
      echo "Opened database successfully\n";
	  echo "<br>";
   }
   
 ///////////////////////////////////////////////////  
   
   if($name) {
  
    $sql =<<<EOF
      SELECT * from member
	  where  m_membername = '$name'
	  and
	  m_password = '$pass';
EOF;


   $ret = $db->query($sql);
   while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
      echo "m_memberkey = ". $row['m_memberkey'] . "\r\n";
      echo "<br>";
	  echo "m_membername = ". $row['m_membername'] ."\t";
	  echo "<br>";
      echo "m_officerkey = ". $row['m_officerkey'] ."\n";
	  echo "<br>";
      echo "m_email =  ".$row['m_email'] ."\n\n";
	  echo "<br>";
	  echo "<br>";
	  
	  $m_memberkey =$row['m_memberkey'];
	  $m_membername =  $row['m_membername'];
	  //$GLOBALS["m_memberkey"] =  $row['m_memberkey'];
	  //$GLOBALS["m_membername"] =  $row['m_membername'];
	  //$_SESSION["name"] = $row['m_membername'];
	  $_SESSION['m_membername'] = $row['m_membername'];
	  $_SESSION['m_memberkey'] = $row['m_memberkey'];
   }
   
   if( $m_memberkey ){
   //echo "Operation done successfully\n";
   echo " <B> equipment owned: </b>" ; 
   echo "<br>";
   $sql =<<<EOF
      SELECT * from equipment
	  where  eq_memberkey = $m_memberkey;
EOF;

    $ret = $db->query($sql);
	
   while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
    echo "eq_eqkey = ". $row['eq_eqkey'] . "\r\n";
      echo "<br>";
	echo "eq_type = ". $row['eq_type'] . "\r\n";
      echo "<br>";
	}
	}
	
	$db->close();
   
}
   
?>
</div>

</body>
</html>