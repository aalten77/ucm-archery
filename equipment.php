<!DOCTYPE html>
<html lang="en">
<head>
    <?php
        session_start();
        header("Cache-Control: no-cache, no-store, must-revalidate");
        //$_SESSION["name"];
    ?>

    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/darkly/bootstrap.min.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="./css/prettify-1.0.css" rel="stylesheet">
    <link href="./css/base.css" rel="stylesheet">
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>


    <title>UC Merced Archery</title>
</head>
<style type="text/css">
    body{ background: #DCDCDC;
        color: black;
    }

    .navbar-default .navbar-brand:hover,
    .navbar-default .navbar-brand:focus {
        color: #FFFFFF;
    }
    .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
        color: #0099CC;
    }
    .nopadding {
        padding: 0 !important;
        margin: 0 !important;
    }

    .navbar-brand{
        padding: 0;
        margin: 0;
    }
    .navbar-brand img
    {
        max-height: 100%;
    }
    .table > thead > tr > th {
        vertical-align: bottom;
        border-bottom: 2px solid #286090;
    }
    .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #286090;
    }
    .form-control{
        width: 50%;
    }
    label.dateheaders{
        max-width: 25%;
        margin-right: 30%;
        margin-left: 0%
        float: left;
        clear: both;
    }
</style>
<body>
<!-- NavBar -->
<nav class="navbar navbar-default">
    <div class="container-fluid">

        <!--Logo -->
        <div class="navbar-header">
            <a href="#" class="navbar-brand"><img class="img-responsive" src="images\UCMArchery.jpg"></a>
        </div>

        <!-- Menu Items -->
        <div>
            <ul class="nav navbar-nav">
                <li><a href="index.html">Home</a></li>
				<li><a href="clubinfo.html">Club Info</a></li>
                <li><a href="Join.php">Join</a></li>
				<li><a href="login.php">Login</a></li>
                <li><a href="photos.html">Photos</a></li>
                <li><a href="budget.php">Budget</a></li>
                <li><a href="tourneyresults.php">Tournament Results</a></li>
                <li class="active"><a href="equipment.php">Equipment</a></li>
                <li><a href="aboutus.html">About Us</a></li>
				<li> 
				<form method="post" id="logout" action='logout.php' >
                    <input id='button' class="btn btn-info btn-sm" type='submit' name='button' value='LOGOUT'/>
				</form>
				</li>
            </ul>
        </div>
    </div>
</nav>


<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th></th>
            <th>Equipment Type</th>
            <th>Checkout Date/Time</th>
            <th>Check-in Date/Time</th>
            <th>Comments</th>
            <th>Member Key</th>
            <th>Member Owns</th>
        </tr>
        </thead>
        <tbody>


        <?php


        class MyDB extends SQLite3
        {
            function __construct()
            {
                $this->open('archerydata.db');
            }
        }
        $db = new MyDB();
        if(!$db){
            echo $db->lastErrorMsg();
        }

        $sql =<<<EOF
      SELECT *
      FROM equipment;
EOF;


        $ret = $db->query($sql);
        while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
            ?>

            <tr>
                <td><?php echo $row['eq_eqkey']; ?></td>
                <td><?php echo $row['eq_type']; ?></td>
                <td><?php echo $row['eq_checkoutdatetime']; ?></td>
                <td><?php echo $row['eq_checkindatetime']; ?></td>
                <td><?php echo $row['eq_comments']; ?></td>
                <td><?php echo $row['eq_memberkey']; ?></td>
                <td><?php echo $row['eq_memberOwns']; ?></td>
            </tr>

            <?php
        }


        ?>
        </tbody>
    </table>
</div>

<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

    <?php
        $sql =<<<EOF
            SELECT eq_eqkey
            FROM equipment;
EOF;
        $ret = $db->query($sql);
    ?>
            <label for="sel1">Select equipment key:</label>
            <div class="form-group">
                <select class="form-control" id="sel1" name="eqkey">
                    <?php   while($row = $ret->fetchArray(SQLITE3_ASSOC)) { ?>
                    <option value="<?php echo $row['eq_eqkey']; ?>"><?php echo $row['eq_eqkey']; ?></option>
                        <?php
                            } ?>
                </select>
            </div>



    <br>

    <label class="dateheaders" for="sel1">Check Out Date/Time: </label><label class="dateheaders" for="sel1">Check In Date/Time: </label>
    <div class="container">

        <div class='col-md-5'>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker6'>
                    <input type='text' class="form-control" name="out_datetime"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                </div>
            </div>
        </div>

        <div class='col-md-5'>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker7'>
                    <input type='text' class="form-control" name="in_datetime"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker6').datetimepicker();
            $('#datetimepicker7').datetimepicker({
                useCurrent: false //Important! See issue #1075
            });
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>

    <br>

    <button type="submit" class="btn btn-primary">Submit</button>


</form>

<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
<div class="form-group">
    <label for="sel1">Select equipment type:</label>
    <select class="form-control" id="sel1" name="eqmttype">
        <option value="Recurve riser">Recurve riser</option>
        <option value="Recurve limbs">Recurve limbs</option>
        <option value="Arm guard">Arm guard</option>
        <option value="Finger tab">Finger tab</option>
        <option value="Arrows (1 doz.)">Arrows (1 doz.)</option>
        <option value="Arrows (1/2 doz.)">Arrows (1/2 doz.)</option>
        <option value="Quiver">Quiver</option>
        <option value="Stabilizer">Stabilizer</option>
        <option value="Sight">Sight</option>
        <option value="Clicker">Clicker</option>
        <option value="Bow stringer">Bow stringer</option>
        <option value="Fletching jig">Fletching jig</option>
        <option value="Propane tank">Propane tank</option>
    </select>
</div>

    <?php

    $sel_eqkey= $_POST['eqkey'];
    $sel_outdatetime= $_POST['out_datetime'];
    $sel_indatetime= $_POST['in_datetime'];

    $out_date = new DateTime($sel_outdatetime);
    $in_date = new DateTime($sel_indatetime);
    $out = $out_date->format("Y-m-d H:i");
    $in = $in_date->format("Y-m-d H:i");

    echo $sel_eqkey;
    echo $sel_outdatetime;
    echo $sel_indatetime;

    //UPDATE Check Out/Check In eqmt
    $sql =<<<EOF
            UPDATE equipment
                SET eq_checkoutdatetime = '$sel_outdatetime', eq_checkindatetime = '$sel_indatetime'
                WHERE eq_eqkey = $sel_eqkey;
EOF;

    $ret = $db->exec($sql);
    if(!$ret){
        echo $db->lastErrorMsg();
    } else {
        echo $db->changes(), "Equipment updated correctly\n";
    }

    ?>


    <label for="sel1">Comment:</label>
    <input type="text" class="form-control" name="comment" />

    <!--
    <textarea rows="4" cols="50" name="comment" form="usrform">
        Enter text here...</textarea>

    <br> -->
    <br>
    <label for="sel1">Memberkey:</label>
    <input type="number" class="form-control" name="memberkey" />
    <br>

    <label for="sel1">Member Owns their own equipment?:</label>
    <br>
    <input type="radio" name="memberowns" value="1" checked>Yes
    <br>
    <input type="radio" name="memberowns" value="0">No
    <br>
    <br>

    <button type="submit" class="btn btn-primary">Submit</button>



</form>

<?php
    $sel_eqmttype= $_POST['eqmttype'];
    $sel_comment= $_POST['comment'];
    $sel_memberkey= $_POST['memberkey'];
    $sel_memberowns= $_POST['memberowns'];


    echo $sel_eqmttype;
    echo $sel_comment;


// INSERT NEW EQUIPMENT SQL
    $sel_memberkey = $_SESSION["m_memberkey"];
    echo $sel_memberkey;
    echo $sel_memberowns;
    if($sel_memberkey) {
        $sql = <<<EOF
        INSERT INTO equipment(eq_type, eq_comments, eq_memberkey, eq_memberOwns)
            VALUES('$sel_eqmttype', '$sel_comment', $sel_memberkey, $sel_memberowns);
EOF;

        $ret = $db->exec($sql);
        if (!$ret) {
            echo $db->lastErrorMsg();
        } else {
            echo "Equipment inputted correctly\n";
        }

    }


?>

<?php
    $db->close();
?>





</body>
</html>